#!/usr/bin/python3
import asyncio
import random


async def say(word):
    wait_time = random.randint(0,5)/2.0
    await asyncio.sleep(wait_time)
    print(word)


async def main():
    tasks = [say(word) for word in ("Let's", 'make', 'it', 'asynchronous')]

    await asyncio.gather(*tasks)


asyncio.run(main())
