#!/usr/bin/python3

from urllib.parse import urlparse
import asyncio
import json
import os.path
import signal
import subprocess
import sys

import aiohttp


with open('links.txt') as links_file:
    links = links_file.readlines()
    links = [link.strip() for link in links]


async def download_mp3(link: str):
    """
    Download the given link, turn it into an ogg, and test it.

    Args:
        link: The link you want to download.
    """
    filename = urlparse(link).path.split('/')[-1]

    async with aiohttp.ClientSession() as session:
        async with session.get(link) as response:
            print('Downloading {}'.format(link))

            with open(filename, 'wb') as the_file:
                the_file.write(await response.read())

        print('\tSaved as: "{}"'.format(filename))

        print('Successfully downloaded {}'.format(link))


async def main():
    tasks = [download_mp3(link) for link in links]

    await asyncio.gather(*tasks)


asyncio.run(main(), debug=True)
