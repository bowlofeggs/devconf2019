#!/usr/bin/python3
# Copyright © 2019 Red Hat, Inc.
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; version 3
# of the License.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

from urllib.parse import urlparse
import asyncio
import json
import os.path
import signal
import subprocess
import sys

import aiohttp


with open('links.txt') as links_file:
    links = links_file.readlines()
    links = [link.strip() for link in links]


async def download_mp3(link: str):
    """
    Download the given link, turn it into an ogg, and test it.

    Args:
        link: The link you want to download.
    """
    filename = urlparse(link).path.split('/')[-1]

    async with download_concurrency_semaphore:
        if not os.path.exists(filename):
            async with aiohttp.ClientSession() as session:
                async with session.get(link) as response:
                    print('Downloading {}'.format(link))

                    with open(filename, 'wb') as the_file:
                        the_file.write(await response.read())

                print('\tSaved as: "{}"'.format(filename))
                if response.status != 200:
                    print(await response.text())
                    raise Exception(
                        'Non-200 status code: {} ({})'.format(response.status, link))

                print('Successfully downloaded {}'.format(link))

    await process_mp3(filename)


async def download_youtube(link):
    async with download_concurrency_semaphore:
        command = ['youtube-dl', '--print-json', '-f', 'bestvideo[height=1080]+bestaudio', link]
        print('Running {}'.format(' '.join(command)))
        process = await asyncio.create_subprocess_exec(
            *command, stdout=subprocess.PIPE)

        stdout, stderr = await process.communicate()

        if process.returncode != 0:
            raise Exception('Non-0 exit code: {} ({})'.format(process.returncode, link))

        filename = json.loads(stdout.strip())['_filename']

        print('Successfully downloaded {}'.format(link))

    await test_media(filename)


async def test_media(filename, delete=None):
    async with audio_concurrency_semaphore:
        await asyncio.sleep(1)

        print('Playing {}'.format(filename))
        command = ['mpv', '--no-terminal', '--start', '33%', '--length', '2', filename]
        print('Running {}'.format(' '.join(command)))
        try:
            process = await asyncio.create_subprocess_exec(
                    *command, stdout=subprocess.PIPE, stderr=subprocess.STDOUT)

            stdout, stderr = await asyncio.wait_for(process.communicate(), 4)

            if process.returncode:
                print(stdout)
                raise Exception('Non-0 exit code: {} ({})'.format(process.returncode, command))
        except asyncio.TimeoutError:
            # mpv does not always exit after 10 seconds on some files. That's OK as long as we hear
            # audio.
            process.send_signal(signal.SIGINT)

            stdout, stderr = await process.communicate()

        print('Successfully tested {}'.format(filename))

        if False and delete:
            os.remove(delete)


async def process_mp3(mp3_filename):
    async with processor_concurrency_semaphore:
        print('Processing {}'.format(mp3_filename))
        name, extension = os.path.splitext(mp3_filename)
        ogg_filename = '{}.ogg'.format(name)
        command = ['ffmpeg', '-nostats', '-loglevel', '8', '-i', mp3_filename, ogg_filename]
        print('Running {}'.format(' '.join(command)))
        process = await asyncio.create_subprocess_exec(*command)

        stdout, stderr = await process.communicate()

        if process.returncode != 0:
            raise Exception('Non-0 exit code: {}'.format(process.returncode))

        print('Successfully processed {}'.format(mp3_filename))

    await test_media(ogg_filename, delete=mp3_filename)


async def main():
    global audio_concurrency_semaphore
    global download_concurrency_semaphore
    global processor_concurrency_semaphore
    audio_concurrency_semaphore = asyncio.Semaphore(value=1)
    download_concurrency_semaphore = asyncio.Semaphore(value=6)
    processor_concurrency_semaphore = asyncio.Semaphore(value=4)

    tasks = []

    errors = ''

    for link in links:
        if 'youtube' in urlparse(link).netloc:
            tasks.append(download_youtube(link))
        else:
            tasks.append(download_mp3(link))

    await asyncio.gather(*tasks)


asyncio.run(main(), debug=True)
