#!/usr/bin/python3
import asyncio
import random


async def say(word):
    wait_time = random.randint(0,5)/2.0
    print("I will say {} after {} seconds.".format(word, wait_time))
    await asyncio.sleep(wait_time)
    if word == 'it':
        raise Exception("I just can't say it.")
    print(word)
    return word


async def main():
    tasks = [say(word) for word in ("Let's", 'make', 'it', 'asynchronous')]

    results = await asyncio.gather(*tasks)

    print(results)


asyncio.run(main())
