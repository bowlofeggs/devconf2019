#!/usr/bin/python3

from urllib.parse import urlparse
import os
import requests
import subprocess


with open('links.txt') as links_file:
    links = links_file.readlines()
    links = [link.strip() for link in links]


for link in links:
    print('Downloading {}'.format(link))

    response = requests.get(link)

    if not response.ok:
        raise Exception('Oh no! {}'.format(response.status))

    mp3_filename = urlparse(link).path.split('/')[-1]
    with open(mp3_filename, 'wb') as the_file:
        the_file.write(response.content)

    print('\tSaved as: "{}"'.format(mp3_filename))

    print('Processing {}'.format(mp3_filename))

    name, extension = os.path.splitext(mp3_filename)
    ogg_filename = '{}.ogg'.format(name)
    command = ['ffmpeg', '-nostats', '-loglevel', '8', '-i', mp3_filename, ogg_filename]

    print('Running {}'.format(' '.join(command)))
    subprocess.check_call(command)
    print('Successfully processed {}'.format(mp3_filename))

    print('Playing {}'.format(ogg_filename))
    command = ['mpv', '--no-terminal', '--start', '33%', '--length', '2', ogg_filename]
    print('Running {}'.format(' '.join(command)))
    subprocess.check_call(command)
